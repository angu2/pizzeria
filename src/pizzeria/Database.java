package pizzeria;

import models.ModelDiscount;
import models.ModelMenu;
import models.ModelOrder;
import models.ModelPurchase;

import java.sql.*;
import java.util.ArrayList;

public class Database {

    private Connection connectClient = null;
    private Connection connectManager = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    public Database () {

    }

    public void CreateConnectionClient() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connectClient = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizzeria?user=klient&password=klient&useLegacyDatetimeCode=false&serverTimezone=UTC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void CreateConnectionManager() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connectManager = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizzeria?user=kierownik&password=kierownik&useLegacyDatetimeCode=false&serverTimezone=UTC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void Znajdz() throws Exception {
//        ResultSet resultSet = null;
//
//
//            this.CreateConnectionClient();
//
//            statement = connectClient.createStatement();
//
//            String sql = "SELECT * FROM dane_osobowe WHERE ";
//            sql += "Imię LIKE '%Ania%'";
//            resultSet = statement.executeQuery(sql);
//
//            resultSet.first();
//            System.out.print(resultSet.getString("Imię"));

    }

    public ArrayList<ModelMenu> GetMenu () throws Exception {
        ResultSet resultSet = null;
        ArrayList listMenu = new ArrayList<ModelMenu>();
        try {
            this.CreateConnectionClient();

            statement = connectClient.createStatement();

            String sql = "SELECT * FROM menu";
            resultSet = statement.executeQuery(sql);

            while (resultSet.next() ) {
                System.out.println(resultSet.getString("Nazwa"));
                ModelMenu menu = new ModelMenu();
                menu.setId(resultSet.getInt("Produkt_ID"));
                menu.setType(resultSet.getString("Typ"));
                menu.setName(resultSet.getString("Nazwa"));
                menu.setPrice(resultSet.getFloat("Cena_katalogowa"));
                menu.setDescription(resultSet.getString("Opis"));

                listMenu.add(menu);
                //System.out.println(menu.getName());
               // menuList.add(new ModelMenu(resultSet.getInt("Produkt_ID"), resultSet.getString("Typ"), resultSet.getString("Nazwa"), resultSet.getFloat("Cena_katalogowa"), resultSet.getString("Opis") )) ;
            }

            //ModelMenu cos = (ModelMenu) listMenu.get(0);
            //System.out.println(cos.getName());

            return  listMenu; //(ArrayList<ModelMenu>) kastowanie

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public ArrayList<ModelDiscount> GetDiscounts() throws Exception{
        ResultSet resultSet = null;
        ArrayList listDiscounts = new ArrayList<ModelDiscount>();
        try {
            this.CreateConnectionClient();

            statement = connectClient.createStatement();

            String sql = "SELECT * FROM rabaty";
            resultSet = statement.executeQuery(sql);

            while (resultSet.next() ) {
                //System.out.println(resultSet.getString("Nazwa"));
                ModelDiscount discount = new ModelDiscount();
                discount.setDiscountId(resultSet.getInt("Rabat_ID"));
                discount.setName(resultSet.getString("Nazwa"));
                discount.setDiscount(resultSet.getString("Zniżka"));

                listDiscounts.add(discount);
            }

            return  listDiscounts; //(ArrayList<ModelMenu>) kastowanie

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public void AddOrder(ModelOrder order) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnectionManager();

            preparedStatement = connectManager
                    .prepareStatement("insert into zamówienia (Klient_ID, Rabat_ID, Kwota_do_zapłaty,  " +
                            "Na_wynos, Data_zamówienia, Faktura, Uwagi_zamówienia, Pracownik_lokalu_ID, Kierowca_ID, Kucharz_ID)" +
                             "values (?,?,?,?,?,?,?,?,?,?)");

            // Parameters start with 1
            preparedStatement.setInt(1, order.getClientId());
            preparedStatement.setInt(2, order.getDiscountId());
            preparedStatement.setFloat(3, order.getTotalPrice());
            preparedStatement.setString(4, order.getTakeAway());
            preparedStatement.setDate(5, Date.valueOf(order.getDate()));
            preparedStatement.setString(6, order.getBill());
            preparedStatement.setString(7, order.getInfo());
            preparedStatement.setInt(8, order.getEmpId());
            preparedStatement.setInt(9, order.getDriverId());
            preparedStatement.setInt(10, order.getCookId());


            preparedStatement.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public void AddMenu (ModelMenu menu) throws Exception{
        ResultSet resultSet = null;
        try {

            this.CreateConnectionManager();

            preparedStatement = connectManager
                    .prepareStatement("insert into menu (Typ, Nazwa, Cena_katalogowa, Opis)" +
                            "values (?,?,?,?)");

            // Parameters start with 1
            preparedStatement.setString(1, menu.getType());
            preparedStatement.setString(2,menu.getName());
            preparedStatement.setFloat(3,menu.getPrice());
            preparedStatement.setString(4,menu.getDescription());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public void AddPurchase (ModelPurchase purchase) throws Exception{
        ResultSet resultSet = null;
        try {

            this.CreateConnectionClient();

            preparedStatement = connectClient
                    .prepareStatement("insert into zakup (Zamówienie_ID, Produkt_ID, Ilość_sztuk, Uwagi  " +
                            "values (?,?,?,?)");

            // Parameters start with 1
            preparedStatement.setInt(1, purchase.getOrderId());
            preparedStatement.setInt(2,purchase.getProductId());
            preparedStatement.setInt(3,purchase.getAmount());
            preparedStatement.setString(4, purchase.getDetails());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public void DeleteMenu (ModelMenu menu) throws SQLException {
        ResultSet resultSet = null;
        try {

            this.CreateConnectionManager();

            preparedStatement = connectManager
                    .prepareStatement("DELETE FROM menu WHERE Produkt_ID = ?");
            preparedStatement.setInt(1, menu.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public void UpdateMenu (ModelMenu menu) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnectionManager();

            preparedStatement = connectManager
                    .prepareStatement("update menu set Typ = ?, Nazwa=?, Cena_katalogowa=?, Opis=? WHERE Produkt_ID = ?");

            // Parameters start with 1
            preparedStatement.setString(1, menu.getType());
            preparedStatement.setString(2,menu.getName());
            preparedStatement.setFloat(3,menu.getPrice());
            preparedStatement.setString(4,menu.getDescription());
            preparedStatement.setInt(5,menu.getId());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    // You need to Close the resultSet
    private void Close(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connectClient != null) {
                connectClient.close();
            }
        } catch (Exception e) {

        }
    }
}
