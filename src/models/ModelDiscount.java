package models;

import javafx.beans.property.*;

public class ModelDiscount {
    private final IntegerProperty discountId = new SimpleIntegerProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty discount = new SimpleStringProperty();

    public int getDiscountId() {
        return discountId.get();
    }

    public IntegerProperty discountIdProperty() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId.set(discountId);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDiscount() {
        return discount.get();
    }

    public StringProperty discountProperty() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount.set(discount);
    }
}
