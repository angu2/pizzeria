package models;

import javafx.beans.property.*;

public class ModelOrder {
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final IntegerProperty clientId = new SimpleIntegerProperty();
    private final IntegerProperty discountId = new SimpleIntegerProperty();
    private final FloatProperty totalPrice = new SimpleFloatProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty takeAway = new SimpleStringProperty();
    private final StringProperty date = new SimpleStringProperty();
    private final StringProperty bill = new SimpleStringProperty();
    private final StringProperty info = new SimpleStringProperty();
    private final IntegerProperty empId = new SimpleIntegerProperty();
    private final IntegerProperty driverId = new SimpleIntegerProperty();
    private final IntegerProperty cookId = new SimpleIntegerProperty();

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getClientId() {
        return clientId.get();
    }

    public IntegerProperty clientIdProperty() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId.set(clientId);
    }

    public int getDiscountId() {
        return discountId.get();
    }

    public IntegerProperty discountIdProperty() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId.set(discountId);
    }

    public float getTotalPrice() {
        return totalPrice.get();
    }

    public FloatProperty totalPriceProperty() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice.set(totalPrice);
    }

    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getTakeAway() {
        return takeAway.get();
    }

    public StringProperty takeAwayProperty() {
        return takeAway;
    }

    public void setTakeAway(String takeAway) {
        this.takeAway.set(takeAway);
    }

    public String getDate() {
        return date.get();
    }

    public StringProperty dateProperty() {
        return date;
    }

    public void setDate(String date) {
        this.date.set(date);
    }

    public String getBill() {
        return bill.get();
    }

    public StringProperty billProperty() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill.set(bill);
    }

    public int getEmpId() {
        return empId.get();
    }

    public IntegerProperty empIdProperty() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId.set(empId);
    }

    public int getDriverId() {
        return driverId.get();
    }

    public IntegerProperty driverIdProperty() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId.set(driverId);
    }

    public int getCookId() {
        return cookId.get();
    }

    public IntegerProperty cookIdProperty() {
        return cookId;
    }

    public void setCookId(int cookId) {
        this.cookId.set(cookId);
    }

    public String getInfo() {
        return info.get();
    }

    public StringProperty infoProperty() {
        return info;
    }

    public void setInfo(String info) {
        this.info.set(info);
    }
}