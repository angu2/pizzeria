package GUI;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javafx.event.ActionEvent;
import models.ModelMenu;
import pizzeria.Database;

import java.util.ArrayList;
import java.util.Optional;

public class ManagerView extends Application {

    Database database = new Database();

    @FXML
    private TableView<ModelMenu> tableViewManagerMenu;
    @FXML private TableColumn<ModelMenu, Integer> colId = new TableColumn<ModelMenu, Integer>();
    @FXML private TableColumn<ModelMenu, String> colType = new TableColumn<ModelMenu, String>();
    @FXML private TableColumn<ModelMenu, String> colName= new TableColumn<ModelMenu, String>();
    @FXML private TableColumn<ModelMenu, Float> colPrice= new TableColumn<ModelMenu, Float>();
    @FXML private TableColumn<ModelMenu, String> colDescription= new TableColumn<ModelMenu, String>();

    @FXML private Button buttonManagerAddProduct;
    @FXML private Button buttonManagerUpdateProduct;
    @FXML private Button buttonManagerDeleteProduct;

    //do zmiany trybu okienka MenuNewProduct
    @FXML private Button buttonAddNewProduct;
    @FXML private Button buttonUpdateNewProduct;
    @FXML private Label labelNewProductTitle;

    //formularz nowego produktu
    @FXML private TextField textFieldNewProductType;
    @FXML private TextField textFieldNewProductName;
    @FXML private TextField textFieldNewProductPrice;
    @FXML private TextArea textAreaNewProductDescription;

    ObservableList<ModelMenu> menuObservableList;
    static int selectedIndex;
    static int listIndex;
    static ModelMenu staticMenu;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

    }

    public void InitData() throws Exception {
        ArrayList<ModelMenu> tmp = new ArrayList();
        tmp = database.GetMenu();

        //final ObservableList<ModelMenu> menuObservableList = FXCollections.observableArrayList(tmp);
        menuObservableList = FXCollections.observableArrayList(tmp);

        colId.setCellValueFactory(new PropertyValueFactory<ModelMenu, Integer>("id"));

        colType.setCellValueFactory(new PropertyValueFactory<ModelMenu, String>("type"));
        colName.setCellValueFactory(new PropertyValueFactory<ModelMenu, String >("name"));
        colPrice.setCellValueFactory(new PropertyValueFactory<ModelMenu,Float>("price"));
        colDescription.setCellValueFactory(new PropertyValueFactory<ModelMenu,String>("description"));
        tableViewManagerMenu.setItems(menuObservableList);

        //odblokowuje przyciski po wybranniu konkretnego produktu
        buttonManagerUpdateProduct.disableProperty().bind(Bindings.equal(-1,tableViewManagerMenu.getSelectionModel().selectedIndexProperty()));
        buttonManagerDeleteProduct.disableProperty().bind(Bindings.equal(-1,tableViewManagerMenu.getSelectionModel().selectedIndexProperty()));
    }

    //inicjuje dane dla okienka MenuNewProduct po Add
    public void InitModalDataAdd(ObservableList obsList) throws Exception {
        menuObservableList = obsList;
        labelNewProductTitle.textProperty().setValue("Dodaj produkt do menu");
        buttonAddNewProduct.setVisible(true);
        buttonUpdateNewProduct.setVisible(false);
    }

    //inicjuje dane dla okienka MenuNewProduct po Update
    public void InitModalDataUpdate(ObservableList obsList) throws Exception {
        menuObservableList = obsList;
        //selectedIndex = index;
        labelNewProductTitle.textProperty().setValue("Aktualizuj produkt z menu");
        buttonAddNewProduct.setVisible(false);
        buttonUpdateNewProduct.setVisible(true);

        //ustawia tekst w textFieldach
        textFieldNewProductType.textProperty().setValue(staticMenu.getType());
        textFieldNewProductName.textProperty().setValue(staticMenu.getName());
        textFieldNewProductPrice.textProperty().setValue( String.valueOf(staticMenu.getPrice()) );
        textAreaNewProductDescription.textProperty().setValue(staticMenu.getDescription());

    }

    //przycisk 'dodaj produkt', otwiera okienko formularza
    public void AddProduct(ActionEvent event) throws Exception {
        ShowMenuNewProductWindow(true);
    }

    //przycisk 'aktualizuj produkt', otwiera okienko formularza z wypełnionymi danymi, zmienic guzik!
    public void UpdateProduct(ActionEvent event) throws Exception {
        ModelMenu menu = tableViewManagerMenu.getSelectionModel().getSelectedItem();
        selectedIndex = menu.getId();
        listIndex = tableViewManagerMenu.getSelectionModel().getSelectedIndex();
        staticMenu = menu;
        ShowMenuNewProductWindow(false);
//        labelNewProductTitle.textProperty().setValue("Aktualizuj produkt z menu");
//        buttonAddNewProduct.setVisible(false);
//        buttonUpdateNewProduct.setVisible(true);
        //uzupelnic textFieldy danymi
    }

    //przycisk 'usun produkt', usuwa z listy i bazy
    public void DeleteProduct(ActionEvent event) throws Exception {
        Alert alertConfirm = new Alert(Alert.AlertType.CONFIRMATION);
        alertConfirm.setHeaderText("Czy na pewno chcesz usunąć ten produkt?");
        Optional<ButtonType> result = alertConfirm.showAndWait();

        if (result.get() == ButtonType.OK)
        {
            //ModelMenu menu = new ModelMenu();
            ModelMenu menu = tableViewManagerMenu.getSelectionModel().getSelectedItem();
            int selectedIndex = tableViewManagerMenu.getSelectionModel().getSelectedIndex();
            database.DeleteMenu(menu);
            tableViewManagerMenu.getItems().remove(selectedIndex);
            //tableViewManagerMenu.refresh();
            //tableViewManagerMenu.getColumns().get(menu.getId()).setVisible(false);
            //tableViewManagerMenu.getColumns().get(menu.getId()).setVisible(true);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Produkt został usunięty z Menu");
            alert.showAndWait();
        }
    }

    //widok MenuNewProduct, 'dodaj nowy produkt'
    public void AddNewProduct (ActionEvent event) throws Exception {
        if (textFieldNewProductType.textProperty().getValue().equals("") || textFieldNewProductName.textProperty().getValue().equals("")
                || textFieldNewProductPrice.textProperty().getValue().equals("")
                || !textFieldNewProductPrice.textProperty().getValue().matches("\\d+(\\.\\d{1,2})?"))
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("ERROR. Błąd danych");
            alert.setContentText("Pola: Typ, Nazwa, Cena nie mogą być puste! \n" +
                                 "Pole: Cena musi zawierać liczbę!");
            alert.showAndWait();
        }
        else
        {
            ModelMenu menu = new ModelMenu();
            menu.setType(textFieldNewProductType.textProperty().getValue());
            menu.setName(textFieldNewProductName.textProperty().getValue());
            float price = Float.parseFloat(textFieldNewProductPrice.textProperty().getValue());
            menu.setPrice(price);
            menu.setDescription(textAreaNewProductDescription.textProperty().getValue());
            database.AddMenu(menu);
            ArrayList<ModelMenu> list = database.GetMenu();
            //menu.setId(database.GetMenu().get(list.size() - 1).getId());
            menu.setId(list.get(list.size() - 1).getId());

            menuObservableList.add(menu);
            //((Node) event.getSource()).getScene().getWindow().hide();
            Stage stage = (Stage) (((Node) event.getSource()).getScene().getWindow());
            stage.close();
            //menuObservableList = FXCollections.observableArrayList(database.GetMenu());

            //Alert
        }
    }

    //widok MenuNewProduct, 'anuluj'
    public void CancelNewProduct (ActionEvent event) throws Exception {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    //widok MenuNewProduct, 'aktualizuj produkt'
    public void UpdateNewProduct (ActionEvent event) throws Exception {
        if (textFieldNewProductType.textProperty().getValue().equals("") || textFieldNewProductName.textProperty().getValue().equals("")
                || textFieldNewProductPrice.textProperty().getValue().equals("")
                || !textFieldNewProductPrice.textProperty().getValue().matches("\\d+(\\.\\d{1,2})?"))
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("ERROR. Błąd danych");
            alert.setContentText("Pola: Typ, Nazwa, Cena nie mogą być puste! \n" +
                    "Pole: Cena musi zawierać liczbę!");
            alert.showAndWait();
        }
        else {
            ModelMenu menu = new ModelMenu();
            menu.setType(textFieldNewProductType.textProperty().getValue());
            menu.setName(textFieldNewProductName.textProperty().getValue());
            float price = Float.parseFloat(textFieldNewProductPrice.textProperty().getValue());
            menu.setPrice(price);
            menu.setDescription(textAreaNewProductDescription.textProperty().getValue());
            menu.setId(selectedIndex);
            database.UpdateMenu(menu);

            menuObservableList.set(listIndex, menu);
            //((Node) event.getSource()).getScene().getWindow().hide();
            Stage stage = (Stage) (((Node) event.getSource()).getScene().getWindow());
            stage.close();
        }
    }

    //otwiera 0kno z formularzem
    private void ShowMenuNewProductWindow(Boolean addNew) throws Exception {
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("MenuNewProduct.fxml"));
        Pane root = (Pane) myLoader.load();
        ManagerView controller = myLoader.getController();
        //controller.InitData();
        if (addNew)
            controller.InitModalDataAdd(menuObservableList);
        else
            controller.InitModalDataUpdate(menuObservableList);

        Scene scene = new Scene (root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("PRODUCT");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();
    }
}

//    Alert alert = new Alert(Alert.AlertType.ERROR);
//    alert.setHeaderText("ERROR. Błąd danych");
//    alert.setContentText("Podaj przynajmniej jeden parametr");
//    alert.showAndWait();
//    https://code.makery.ch/blog/javafx-dialogs-official/

//    "\\d+(\\.\\d{1,2})?" min jedna(+) cyfra(\\d) i opcjonalnie(?) kropka(\\.) i 1/2 cyfry({1,2})
//    Regular expressions, String patterns:
//    http://www.vogella.com/tutorials/JavaRegularExpressions/article.html
