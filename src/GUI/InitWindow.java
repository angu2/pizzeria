package GUI;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pizzeria.Database;

public class InitWindow extends Application {

    @FXML
    Label label;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

    }

    @FXML
    public void ClientInit(ActionEvent event) throws Exception {
        Database database = new Database();
        database.Znajdz();

        ((Node) event.getSource()).getScene().getWindow().hide();

        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("ClientView.fxml"));
        Pane root = (Pane) myLoader.load();
        ClientView controller = myLoader.getController();

        controller.InitData();

        Scene scene = new Scene (root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("KLIENT");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(true);
        stage.show();
    }

    @FXML
    public void ManagerInit(ActionEvent event) throws Exception{
        ((Node) event.getSource()).getScene().getWindow().hide();

        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("ManagerView.fxml"));
        Pane root = (Pane) myLoader.load();
        ManagerView controller = myLoader.getController();

        controller.InitData();

        Scene scene = new Scene (root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("MANAGER");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(true);
        stage.show();
    }
}
