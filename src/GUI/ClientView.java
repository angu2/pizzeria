package GUI;

import com.sun.xml.internal.bind.v2.model.core.ID;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import models.ModelDiscount;
import models.ModelMenu;
import models.ModelOrder;
import models.ModelPurchase;
import pizzeria.Database;
import javafx.collections.ObservableList;


import java.io.IOException;
import java.util.ArrayList;

public class ClientView extends Application {

    Database database = new Database();

    ObservableList<ModelMenu> menuObservableList;

    ObservableList<ModelPurchase> purchaseObservableList;

    static ArrayList<ModelDiscount> discountsList;

    @FXML private TableView<ModelMenu> tableViewClientMenu;
    @FXML private TableColumn<ModelMenu, Integer> colId = new TableColumn<ModelMenu, Integer>();
    @FXML private TableColumn<ModelMenu, String> colType = new TableColumn<ModelMenu, String>();
    @FXML private TableColumn<ModelMenu, String> colName= new TableColumn<ModelMenu, String>();
    @FXML private TableColumn<ModelMenu, Float> colPrice= new TableColumn<ModelMenu, Float>();
    @FXML private TableColumn<ModelMenu, String> colDescription= new TableColumn<ModelMenu, String>();

    @FXML private TableView<ModelPurchase> tableViewClientCart;
    @FXML private TableColumn<ModelPurchase, Integer> columnCartProductId = new TableColumn<ModelPurchase,Integer>();
    @FXML private TableColumn<ModelPurchase, Integer> columnCartAmount = new TableColumn<ModelPurchase, Integer>();
    @FXML private TableColumn<ModelPurchase, Float> columnCartPrice = new TableColumn<ModelPurchase, Float>();
    @FXML private TableColumn<ModelPurchase, String> columnCartDetails = new TableColumn<ModelPurchase, String>();

    //add product i order
    @FXML private ChoiceBox<String> choiceBoxClientDiscount;
    @FXML private ChoiceBox<Integer> choiceBoxClientProductAmount;
    @FXML private TextField textFieldClientProductId;
    @FXML private TextArea textAreaClientProductDetails;

    @FXML private CheckBox checkboxClientTakeAway;
    @FXML private CheckBox checkboxClientBill;
    @FXML private TextArea TextAreaClientOrderInfo;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

    }

    public void InitData() throws Exception {
        ArrayList<ModelMenu> tmp = new ArrayList();
        tmp = database.GetMenu();
        menuObservableList = FXCollections.observableArrayList(tmp);

        colId.setCellValueFactory(new PropertyValueFactory<ModelMenu, Integer>("id"));

        colType.setCellValueFactory(new PropertyValueFactory<ModelMenu, String>("type"));
        colName.setCellValueFactory(new PropertyValueFactory<ModelMenu, String >("name"));
        colPrice.setCellValueFactory(new PropertyValueFactory<ModelMenu,Float>("price"));
        colDescription.setCellValueFactory(new PropertyValueFactory<ModelMenu,String>("description"));
       //tableViewClientMenu.getColumns().setAll(colId, colType, colName, colPrice, colDescription);
        tableViewClientMenu.setItems(menuObservableList);

        //tableView KOSZYK - wyswietlanie listy
        purchaseObservableList = FXCollections.observableArrayList();
        columnCartProductId.setCellValueFactory(new PropertyValueFactory<ModelPurchase,Integer>("productId"));
        columnCartAmount.setCellValueFactory(new PropertyValueFactory<ModelPurchase, Integer>("amount"));
        columnCartPrice.setCellValueFactory(new PropertyValueFactory<ModelPurchase, Float>("price"));
        columnCartDetails.setCellValueFactory(new PropertyValueFactory<ModelPurchase, String>("details"));
        tableViewClientCart.setItems(purchaseObservableList);

        //rabaty do choiceBoxa
        discountsList = database.GetDiscounts();
        for (ModelDiscount discount: discountsList)
        {
            choiceBoxClientDiscount.getItems().add(discount.getName());
        }
        //formularz inne
        choiceBoxClientProductAmount.getItems().addAll(1,2,3,4,5,6,7,8,9,10);
        choiceBoxClientProductAmount.setValue(1);

//        tableViewClientMenu.getSelectionModel().select(0);
//        System.out.println(tableViewClientMenu.getSelectionModel().selectedItemProperty().getValue().idProperty());
//        textFieldClientProductId.textProperty().bindBidirectional( tableViewClientMenu.getSelectionModel().selectedItemProperty().getValue().idProperty(), new NumberStringConverter());

        //BINDING do textfielda
        tableViewClientMenu.getSelectionModel().selectedItemProperty()
                .addListener( (ObservableValue, oldModelMenu, newModelMenu) -> {
                    textFieldClientProductId.textProperty().bind(newModelMenu.idProperty().asString());
                } );

    }

    public void ClientShowOrders (ActionEvent event) throws IOException {
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("ClientOrders.fxml"));
        Pane root = (Pane) myLoader.load();
        ClientOrders controller = myLoader.getController();

        controller.InitData();

        Scene scene = new Scene (root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("MOJE ZAMÓWIENIA");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(true);
        stage.show();
    }

    public void ClientOrder (ActionEvent event) throws Exception {
        ModelOrder order = new ModelOrder();
        order.setClientId(1);
        order.setDiscountId(1);
        //order.setTotalPrice((float)20.00);
        if (checkboxClientTakeAway.isSelected())
            order.setTakeAway("TAK");
        else
            order.setTakeAway("NIE");
        order.setDate("2019-01-21");            //Calendar.getInstance()
        if (checkboxClientBill.isSelected())
            order.setBill("TAK");
        else
            order.setBill("NIE");
        order.setInfo(TextAreaClientOrderInfo.textProperty().getValue());
        order.setEmpId(1);
        order.setDriverId(2);
        order.setCookId(3);

        //abaty
        String d =  choiceBoxClientDiscount.getValue();
        for (ModelDiscount item: discountsList)
        {
            if (item.getName() == d)
                order.setDiscountId(item.getDiscountId());
        }

        database.AddOrder(order);

        purchaseObservableList.clear();

        checkboxClientBill.setSelected(false);
        checkboxClientTakeAway.setSelected(false);
        TextAreaClientOrderInfo.textProperty().setValue("");

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Zamówienie zostało złożone");
        alert.showAndWait();
    }

    public void ClientAddProductToCart (ActionEvent event) throws Exception {
        ModelPurchase purchase = new ModelPurchase();
        purchase.setProductId(Integer.valueOf(textFieldClientProductId.textProperty().getValue()) );
        purchase.setAmount(choiceBoxClientProductAmount.getValue());
        purchase.setDetails(textAreaClientProductDetails.textProperty().getValue());

        purchase.setPrice(tableViewClientMenu.getSelectionModel().selectedItemProperty().getValue().getPrice());

        purchase.setOrderId(1);

        purchaseObservableList.add(purchase);

        //textFieldClientProductId.textProperty().setValue(String.valueOf(tableViewClientMenu.getSelectionModel().selectedItemProperty().getValue().getId()));
        choiceBoxClientProductAmount.setValue(1);
        textAreaClientProductDetails.textProperty().setValue("");
    }

}

// dodawanie produktow ktore juz sa w koszyku
// dodawanie zamowienia z danymi z formularza
// sczytanie id nowego zamowienia (jak nowego produktu w menu)
// kazdemu z listy purchase przypisanie orderId i dodanie do bazy
// alert o zlozeniu zamowienia
// wyczyszczenie listy
// wyczyszczzenie textboxow po dodaniu produktu i po zamowieniu